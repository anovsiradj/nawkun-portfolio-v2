<?php
namespace lib;
class Base extends Config {
	public static $lib = array();
	public static $useLayout = true;
	public static $appCurrent = null;
	public $routeList;

	// function __construct() {
	// }

	public function build($opt = array()) {

		// die(static::$appCurrent);

		echo "++ lib\class\Base\n";
		static::index(); //home:index
		if (static::$useLayout) {
			include join(DS,static::$layoutPath);
		} else {
			include join(DS,static::$templatePath).static::$appCurrent.".php";
		}
	}

	public function importLib($obj) {
		static::$lib[$this->objName(get_class($obj))] = $obj;
	}

	public function objName($name) {
		return end(explode("\\", $name));
	}
}