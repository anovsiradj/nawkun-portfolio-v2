<?php
namespace lib;
class meSQL extends Base {
	private $sql;
	private $content;
	private $method;
	private $req;
	function __construct() {
		$con = static::$libOption[$this->objName(get_class($this))];
		try {
			$this->sql = new \mysqli($con['host'],$con['user'],$con['pass'],$con['dbname']);
		} catch (Exception $e) {
			die($e);
		}
		// static::
		// echo "++ lib\class\sql: connect:".json_encode(static::$sql_connect)."\n";
	}
	public static function need($q) {
		$result = array("lorem","ipsum");
		return json_encode($result);
	}
	public function meQuery($q) {
		$this->req = $q;
		$this->content = null;
		$this->method = strtolower(current(explode(" ", $q)));

		// if (!in_array($this->method, array("select"))) {
		// 	die("meSQL:method {$this->method} Not Allowed");
		// }
	}
	public function meResult($opt = null) {
		try {
			$q = $this->sql->query($this->req);
		} catch (Exception $e) {
			die($e);
		}
		switch ($this->method) {
			case 'select':
				if (!empty($q->num_rows)) {
					while ($n = $q->fetch_assoc()) {
						$this->content[] = $n;
					}
				}
			break;
		}
		return $this->content;
	}
}