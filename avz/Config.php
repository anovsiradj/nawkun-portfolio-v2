<?php
namespace avz;
abstract class Config {
	public static $libOption = array(
		"mysqli"=>array(
			"host"=>"localhost",
			"user"=>"root",
			"pass"=>"",
			"dbname"=>"ndv2"
		)
	);
	public static $partialTemplatePath = array("app","template",'partial','');
	public static $templatePath = array("app","template",'');
	public static $errorTemplateFile = array("app","template",'nope.php');
}