<?php
namespace avz;
class Base extends Config {
	public static $lib = array();
	public static $useLayout = true;
	public static $appCurrent = null;
	public $routes = null;
	public static $routeMethod = 1;
	public static $allowBuild = true;
	public static $datas = array();
	public static $template = null;
	public static $templatePartial = array();

	public function build($opt = array()) {
		extract(static::$datas);
		if (empty($this->routes[static::$routeMethod])) {
			static::index();
		} else {
			if (method_exists($this,$this->routes[static::$routeMethod])) {
				static::$this->routes[static::$routeMethod]();
			} else {
				static::$allowBuild = false;
			}
		}
		// die(method_exists($this,'index'));
		// die(get_called_class());
		if (static::$allowBuild) {
			if (!empty(static::$templatePartial['before']) && empty(static::$onlyTemplate)) {
				foreach (static::$templatePartial['before'] as $value) {
					include $value;
				}
			}
			include static::$template;
			if (!empty(static::$templatePartial['after']) && empty(static::$onlyTemplate)) {
				foreach (static::$templatePartial['after'] as $value) {
					include $value;
				}
			}
		} else {
			include join(DS,static::$errorTemplateFile);
		}
		/*
		if (static::$allow) {
			extract(static::$datas);
			if (!empty($this->routes[0])) {
				// static::$this->routes[0]();
			}
			// static::index();
			if (!empty(static::$templatePartial['before']) && empty(static::$onlyTemplate)) {
				foreach (static::$templatePartial['before'] as $value) {
					include $value;
				}
			}
			include static::$template;
			if (!empty(static::$templatePartial['after']) && empty(static::$onlyTemplate)) {
				foreach (static::$templatePartial['after'] as $value) {
					include $value;
				}
			}
		*/
	}

	// public function importLib($obj) {
	// 	static::$lib[$this->objName(get_class($obj))] = $obj;
	// }

	public function data($name,$value = null) {
		if (is_array($name)) {
			foreach ($name as $k => $v) {
				static::$datas[$k] = $v;
			}
		} else {
			static::$datas[$name] = $value;
		}
	}

	public function partialTemplate($position = null,$v = null) {
		if (!empty($v) && !empty($position)) {
			if (is_array($v)) {
				foreach ($v as $val) {
					static::$templatePartial[$position][] = join(DS,static::$partialTemplatePath).$val;
				}
			} else {
				static::$templatePartial[$position][] = join(DS,static::$partialTemplatePath).$v;
			}
		}
	}

	public function template($path) {
		static::$template = join(DS,static::$templatePath).static::$appCurrent.".php";
	}

	public function objName($name) {
		return end(explode("\\", $name));
	}

	public function routeSource($route = null) {
		// die($route);
		if (empty($route)) {
			$this->routes = array("index");
		} else {
			$this->routes = array_filter(explode("/",$route));
		}
		// die(print_r($this->routes));
	}
}