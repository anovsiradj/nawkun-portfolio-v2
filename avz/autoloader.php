<?php
function myloader($shortcut) {
	$path = "";
	$object = explode("\\", $shortcut);
	$type = $object[0];
	$name = end($object);
	switch ($type) {
		case 'avz':
			$path .= $type.DS.$name;
		break;
		case 'lib':
			$path .= "fw".DS.$type.DS.$name;
		break;
		case 'app':
			$role = $object[1]; // guest, user, admin
			$path .= $type.DS."control".DS.$role.DS.$name;
		break;
	}
	$path .= ".php";
	// echo "-- {$path}\n";
	include $path;
}
spl_autoload_register("myloader");
