<pre><?php
define("DS", DIRECTORY_SEPARATOR);
include("avz".DS."autoloader.php");

$base = new avz\Base;
$base->routeSource(empty($_GET['requestroute']) ? 'index' : $_GET['requestroute']);
$base->partialTemplate('before',array('top.php'));
$base->partialTemplate('after',array('bottom.php'));

// print_r($base->routes);

$role = 'guest';
$appname = "app\\{$role}\\{$base->routes[0]}";
$initial = $base->routes[0];

if (in_array($initial,array('index','thread','wpage'))) {
	$app = new $appname;
	if ($initial = 'thread') {
		if (empty($base->routes[1])) {
			$app::$allowBuild = false;
		}
	}
} else {
	$app = $base;
	$app::$allowBuild = false;
}

$app->build();