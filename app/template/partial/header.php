<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="navbar no-mg-y-b no-radius navbar-default" id="core_navbar">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">
			        <img alt="Brand" class="nk-header-logo" src="nawkun-logo-40x40.png"/>
			      </a>
			    </div>

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="#">Portofolio</a></li>
			        <li><a href="#">About</a></li>
			        <li><a href="#">Tutorial</a></li>
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			          	<img alt="Brand" class="nk-header-logo" src="nawkun-logo-40x40.png"/>
			          </a>
			          <ul class="dropdown-menu">
			            <li><a href="#">Action</a></li>
			            <li><a href="#">Another action</a></li>
			            <li><a href="#">Something else here</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="#">Separated link</a></li>
			          </ul>
			        </li>
			      </ul>
			      <form class="navbar-form navbar-right" role="search">
			      	<div class="input-group">
			      	  <input type="text" class="form-control no-more-glow" placeholder="Search for..."/>
			      	  <span class="input-group-btn">
			      	    <button class="btn btn-default no-more-glow" type="button">Go!</button>
			      	  </span>
			      	</div>
			      </form>
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron no-mg-y-b no-radius img-trans" id="core_header" style="background-image: url(jumbotron.jpg);">
				<h2 class="no-mg-y">Achmad Nawfal Karim</h2>
				<p class="no-mg-y">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="navbar no-mg-y-b navbar-default no-radius" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu-art">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">My Works</a>
					</div>
					<div class="collapse navbar-collapse" id="navbar-menu-art">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Smudge</a></li>
							<li><a href="#">Vector</a></li>
							<li><a href="#">Wpap</a></li>
							<li><a href="#">Gads Photo Painting</a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>