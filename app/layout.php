<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Achmad Nawfal Karim Personal Website</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="gudang/core.css"/>
	</head>
	<body class="bg-dark">
<?php /*header_file*/ ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="bg-white">
					<div id="core_content"></div>
						<div id="loading_scope" class="downmore">
							<div class="loading" style="zoom:1.1;"></div>
						</div>
						<div class="text-center downmore" id="loadmore_scope" style="display:none;padding:8px;">
							<button type="button" onclick="toggleingmore();" class="btn btn-xs btn-default" style="border-radius: 50%;border-color:#fff;background-color:transparent;color:#fff;">
								<!-- <span style="padding:0px 1px;">+</span> -->
								<b style="padding:0px 2.5px;">&#65516;</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php /*footer_file*/ ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="gudang/scale.js"></script>
		<script src="gudang/core.js"></script>
	</body>
</html>
<!--
Resource:
MDN Demos. Loading Animation (https://developer.mozilla.org/en-US/demos/detail/pure-css3-animated-loading-icon/launch)
Random Hex. (http://stackoverflow.com/questions/5092808/how-do-i-randomly-generate-html-hex-color-codes-using-javascript)
Symbols Entities. (http://www.amp-what.com/unicode/search/down%20arrow)
Table FullHright (http://stackoverflow.com/questions/17544625/html-table-with-100-of-height-and-the-rows-equally-divided-in-height-how-to-m)
- http://geekgirllife.com/place-text-over-images-on-hover-without-javascript/
-->